import ApolloClient from 'apollo-client/ApolloClient'
import { SubscriptionClient } from 'subscriptions-transport-ws'

declare module 'apollo-client' {
	interface ApolloClient<TCacheShape> {
		wsClient: SubscriptionClient
	}
}

import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import { store } from './store'
import { vueApollo } from './plugins/vue-apollo'

Vue.config.productionTip = false

new Vue({
	router,
	store,
	apolloProvider: vueApollo,
	render: h => h(App)
}).$mount('#app')

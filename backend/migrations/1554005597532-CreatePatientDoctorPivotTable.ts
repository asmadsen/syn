import {MigrationInterface, QueryRunner} from "typeorm";

export class CreatePatientDoctorPivotTable1554005597532 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "patient_doctor" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "relationship" character varying NOT NULL, "patientId" uuid, "doctorId" uuid, CONSTRAINT "PK_e10105a04a8a381baec6ba1fc6a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "patient_doctor" ADD CONSTRAINT "FK_650a7d13bd01e5cde764e6da3ca" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "patient_doctor" ADD CONSTRAINT "FK_fe8a5443c1e29871e3b6386178d" FOREIGN KEY ("doctorId") REFERENCES "doctor"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "patient_doctor" DROP CONSTRAINT "FK_fe8a5443c1e29871e3b6386178d"`);
        await queryRunner.query(`ALTER TABLE "patient_doctor" DROP CONSTRAINT "FK_650a7d13bd01e5cde764e6da3ca"`);
        await queryRunner.query(`DROP TABLE "patient_doctor"`);
    }

}

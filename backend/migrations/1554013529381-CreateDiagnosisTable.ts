import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateDiagnosisTable1554013529381 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "diagnosis_category_enum" AS ENUM('Heart', 'Brain', 'Intestines')`);
        await queryRunner.query(`CREATE TABLE "diagnosis" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "category" "diagnosis_category_enum" NOT NULL, "dateOfDiagnosis" TIMESTAMP NOT NULL, "description" character varying NOT NULL, "patientId" uuid, CONSTRAINT "PK_d5dbb1cc4e30790df368da56961" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "diagnosis" ADD CONSTRAINT "FK_42a05e713569ded2df15668208c" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "diagnosis" DROP CONSTRAINT "FK_42a05e713569ded2df15668208c"`);
        await queryRunner.query(`DROP TABLE "diagnosis"`);
        await queryRunner.query(`DROP TYPE "diagnosis_category_enum"`);
    }

}

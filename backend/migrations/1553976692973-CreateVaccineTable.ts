import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateVaccineTable1553976692973 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query("CREATE TYPE \"vaccine_status_enum\" AS ENUM('Renewable', 'Expired', 'Permanent', 'Ongoing')")
		await queryRunner.query(
			'CREATE TABLE "vaccine" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status" "vaccine_status_enum" NOT NULL, "vaccinationPlan" text NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "expiresAt" TIMESTAMP DEFAULT null, "patientId" uuid, CONSTRAINT "PK_3879829f8d2e396157ebffab918" PRIMARY KEY ("id"))'
		)
		await queryRunner.query(
			'ALTER TABLE "vaccine" ADD CONSTRAINT "FK_cd02dc70dfa579410588f8a93e6" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
		)
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "vaccine" DROP CONSTRAINT "FK_cd02dc70dfa579410588f8a93e6"')
		await queryRunner.query('DROP TABLE "vaccine"')
		await queryRunner.query('DROP TYPE "vaccine_status_enum"')
	}
}

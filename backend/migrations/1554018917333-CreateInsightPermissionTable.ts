import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateInsightPermissionTable1554018917333 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "insight_permission_category_enum" AS ENUM()`);
        await queryRunner.query(`CREATE TABLE "insight_permission" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "category" "insight_permission_category_enum" NOT NULL, "grantedAt" TIMESTAMP NOT NULL, "patientDoctorId" uuid, CONSTRAINT "PK_30697259aea4176ebf4c498a227" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "insight_permission" ADD CONSTRAINT "FK_d5429fe8f1f62f88affd0b7c43c" FOREIGN KEY ("patientDoctorId") REFERENCES "patient_doctor"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "insight_permission" DROP CONSTRAINT "FK_d5429fe8f1f62f88affd0b7c43c"`);
        await queryRunner.query(`DROP TABLE "insight_permission"`);
        await queryRunner.query(`DROP TYPE "insight_permission_category_enum"`);
    }

}

import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateAllergyTable1553968869107 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query("CREATE TYPE \"allergy_allergyseverity_enum\" AS ENUM('Mild Allergy', 'Moderate Allergy', 'Severe Allergy')")
		await queryRunner.query(
			'CREATE TABLE "allergy" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "allergicTo" character varying NOT NULL, "allergySeverity" "allergy_allergyseverity_enum" NOT NULL, "detected" TIMESTAMP NOT NULL, "lastTested" TIMESTAMP NOT NULL, "reaction" character varying NOT NULL, "patientId" uuid, CONSTRAINT "PK_c9cb3ece73ddfde61d2ada768e1" PRIMARY KEY ("id"))'
		)
		await queryRunner.query(
			'ALTER TABLE "allergy" ADD CONSTRAINT "FK_bc3502f7acc8a5575b7185429fa" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
		)
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "allergy" DROP CONSTRAINT "FK_bc3502f7acc8a5575b7185429fa"')
		await queryRunner.query('DROP TABLE "allergy"')
		await queryRunner.query('DROP TYPE "allergy_allergyseverity_enum"')
	}
}

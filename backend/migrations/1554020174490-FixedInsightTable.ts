import {MigrationInterface, QueryRunner} from "typeorm";

export class FixedInsightTable1554020174490 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TYPE "insight_permission_category_enum" RENAME TO "insight_permission_category_enum_old"`);
        await queryRunner.query(`CREATE TYPE "insight_permission_category_enum" AS ENUM('Allergy', 'Diagnosis', 'Prescription', 'Vaccine')`);
        await queryRunner.query(`ALTER TABLE "insight_permission" ALTER COLUMN "category" TYPE "insight_permission_category_enum" USING "category"::"text"::"insight_permission_category_enum"`);
        await queryRunner.query(`DROP TYPE "insight_permission_category_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "insight_permission_category_enum_old" AS ENUM()`);
        await queryRunner.query(`ALTER TABLE "insight_permission" ALTER COLUMN "category" TYPE "insight_permission_category_enum_old" USING "category"::"text"::"insight_permission_category_enum_old"`);
        await queryRunner.query(`DROP TYPE "insight_permission_category_enum"`);
        await queryRunner.query(`ALTER TYPE "insight_permission_category_enum_old" RENAME TO "insight_permission_category_enum"`);
    }

}

import { MigrationInterface, QueryRunner } from 'typeorm'

export class ChangeBloodTypeToEnum1553967736168 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "patient" DROP COLUMN "birthDate"')
		await queryRunner.query('ALTER TABLE "patient" ADD "birthDate" TIMESTAMP NOT NULL')
		await queryRunner.query('ALTER TABLE "patient" DROP COLUMN "bloodType"')
		await queryRunner.query("CREATE TYPE \"patient_bloodtype_enum\" AS ENUM('A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-')")
		await queryRunner.query('ALTER TABLE "patient" ADD "bloodType" "patient_bloodtype_enum" NOT NULL')
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "patient" DROP COLUMN "bloodType"')
		await queryRunner.query('DROP TYPE "patient_bloodtype_enum"')
		await queryRunner.query('ALTER TABLE "patient" ADD "bloodType" character varying NOT NULL')
		await queryRunner.query('ALTER TABLE "patient" DROP COLUMN "birthDate"')
		await queryRunner.query('ALTER TABLE "patient" ADD "birthDate" date NOT NULL')
	}
}

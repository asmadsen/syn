import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreatePrescriptionTable1554002270796 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query(
			'CREATE TABLE "prescription" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, "isMedicationStrong" boolean NOT NULL, "issueDate" TIMESTAMP NOT NULL, "expirationDate" TIMESTAMP NOT NULL, "dosageDirections" character varying NOT NULL, "patientId" uuid, CONSTRAINT "PK_eaba5e4414e5382781e08467b51" PRIMARY KEY ("id"))'
		)
		await queryRunner.query(
			'ALTER TABLE "prescription" ADD CONSTRAINT "FK_d9d1ecabc97e4de5c07a1795279" FOREIGN KEY ("patientId") REFERENCES "patient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
		)
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE "prescription" DROP CONSTRAINT "FK_d9d1ecabc97e4de5c07a1795279"')
		await queryRunner.query('DROP TABLE "prescription"')
	}
}

import {MigrationInterface, QueryRunner} from "typeorm";

export class CreatedCombinedUniqueKeyInPatientDoctor1554006495577 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "patient_doctor" ADD CONSTRAINT "UQ_1ab4ccd9a90a54cd9acbc884e98" UNIQUE ("patientId", "doctorId")`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "patient_doctor" DROP CONSTRAINT "UQ_1ab4ccd9a90a54cd9acbc884e98"`);
    }

}

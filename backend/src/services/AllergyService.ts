import { AfterRoutesInit, Service } from '@tsed/common'
import { Connection } from 'typeorm'
import { TypeORMService } from '@tsed/typeorm'
import { Allergy } from '../entities/Allergy'

@Service()
export class AllergyService implements AfterRoutesInit {
	private connection: Connection

	constructor(private typeORMService: TypeORMService) {}

	$afterRoutesInit(): void | Promise<any> {
		this.connection = this.typeORMService.get()
	}

	async create(allergy: Allergy): Promise<Allergy> {
		return this.connection.manager.save(allergy)
	}

	async findAllByPatientId(patientId: string): Promise<Allergy[]> {
		return this.connection.manager.find(Allergy, {
			where: {
				patient: patientId
			}
		})
	}
}

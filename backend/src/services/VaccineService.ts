import { AfterRoutesInit, Service } from '@tsed/common'
import { TypeORMService } from '@tsed/typeorm'
import { Connection, Repository } from 'typeorm'
import { Vaccine } from '../entities/Vaccine'

@Service()
export class VaccineService implements AfterRoutesInit {
	private connection: Connection
	repository: Repository<Vaccine>
	constructor(private typeORMService: TypeORMService) {}
	$afterRoutesInit(): void | Promise<any> {
		this.connection = this.typeORMService.get()
		this.repository = this.connection.manager.getRepository(Vaccine)
	}
}

import { AfterRoutesInit, Service } from '@tsed/common'
import { ApolloServer } from 'apollo-server-express'
import { GraphQLService as graphQLService } from '@tsed/graphql'

@Service()
export class GraphqlService implements AfterRoutesInit {
	private server: ApolloServer
	constructor(private graphqlService: graphQLService) {}

	$afterRoutesInit(): void | Promise<any> {
		this.server = this.graphqlService.get('server')
	}
}

import { AfterRoutesInit, Service } from '@tsed/common'
import { Connection, EntitySchema, ObjectType, Repository } from 'typeorm'
import { TypeORMService } from '@tsed/typeorm'

@Service()
export class RepositoryService implements AfterRoutesInit {
	private connection: Connection

	constructor(private typeORMService: TypeORMService) {}

	$afterRoutesInit (): void | Promise<any> {
		this.connection = this.typeORMService.get()
	}

	getRepository<Entity>(target: ObjectType<Entity> | EntitySchema<Entity> | string): Repository<Entity> {
		return this.connection.manager.getRepository(target)
	}
}

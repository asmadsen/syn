import { AfterRoutesInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { TypeORMService } from '@tsed/typeorm'
import { Patient } from '../entities/Patient'
import { GraphQLError } from 'graphql'

@Service()
export class PatientService implements AfterRoutesInit {
	private connection: Connection
	repository: Repository<Patient>

	constructor(private typeORMService: TypeORMService) {}

	$afterRoutesInit(): void | Promise<any> {
		this.connection = this.typeORMService.get()
		this.repository = this.connection.manager.getRepository(Patient)
	}

	async create(patient: Patient): Promise<Patient> {
		return this.connection.manager.save(patient)
	}

	async findAll(): Promise<Patient[]> {
		return this.connection.manager.find(Patient)
	}

	async changeOrganDonorState(patientId: string, organDonor: boolean): Promise<Patient> {
		const patient: Patient = await this.connection.manager.findOne(Patient, {
			id: patientId
		})

		if (patient) {
			patient.organDonor = organDonor
			return this.connection.manager.save(patient)
		}

		throw new GraphQLError('Not found')
	}
}

import { AfterRoutesInit, Service } from '@tsed/common'
import { Connection, Repository } from 'typeorm'
import { Prescription } from '../entities/Prescription'
import { TypeORMService } from '@tsed/typeorm'

@Service()
export class PrescriptionService implements AfterRoutesInit {
	private connection: Connection
	repository: Repository<Prescription>

	constructor(private typeORMService: TypeORMService) {}

	$afterRoutesInit(): void | Promise<any> {
		this.connection = this.typeORMService.get()
		this.repository = this.connection.manager.getRepository(Prescription)
	}
}

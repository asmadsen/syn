import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings } from '@tsed/common'
import * as cookieParser from 'cookie-parser'
import * as compress from 'compression'
import * as methodOverride from 'method-override'
import * as bodyParser from 'body-parser'
import { IGraphQLSettings } from '@tsed/graphql'
import { typeorm } from './TypeOrm'
const rootDir = __dirname

@ServerSettings({
	rootDir,
	httpPort: 8081,
	httpsPort: false,
	acceptMimes: ['application/json'],
	componentsScan: ['${rootDir}/middlewares/**/*.ts', '${rootDir}/services/**/*.ts', '${rootDir}/graphql/**/*.ts', '${rootDir}/converters/**/*.ts'],
	graphql: {
		server: {
			path: '/graphql',
			resolvers: ['${rootDir}/graphql/resolvers/**/*.ts'],
			buildSchemaOptions: {
				emitSchemaFile: true
			}
		} as IGraphQLSettings
	},
	typeorm: [typeorm]
})
export class Server extends ServerLoader {
	public $onMountingMiddlewares(): void | Promise<any> {
		this.use(GlobalAcceptMimesMiddleware)
			.use(cookieParser())
			.use(compress())
			.use(methodOverride())
			.use(bodyParser.json())
			.use(
				bodyParser.urlencoded({
					extended: true
				})
			)

		return null
	}

	public $onReady() {
		console.log('Server started...')
	}

	public $onServerInitError(error: any): any {
		console.error(error)
	}
}

new Server().start()

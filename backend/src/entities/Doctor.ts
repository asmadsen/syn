import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { PatientDoctor } from './PatientDoctor'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class Doctor {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field()
	@Column()
	name!: string

	@Field()
	@Column()
	associatedInstitution!: string

	@Field(type => [PatientDoctor])
	@OneToMany(type => PatientDoctor, relationship => relationship.doctor, { lazy: true })
	patientRelations!: Lazy<PatientDoctor[]>
}

@InputType()
export class DoctorInput implements Partial<Doctor> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	name!: string

	@Field()
	associatedInstitution!: string
}

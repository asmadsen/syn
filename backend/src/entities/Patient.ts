import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Field, ID, InputType, ObjectType } from 'type-graphql'
import { Column } from 'typeorm/decorator/columns/Column'
import { BloodType } from './BloodType'
import { Allergy } from './Allergy'
import { Lazy } from '../helpers/Lazy'
import { Vaccine } from './Vaccine'
import { Prescription } from './Prescription'
import { PatientDoctor } from './PatientDoctor'
import { Diagnosis } from './Diagnosis'

@Entity()
@ObjectType()
export class Patient {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field()
	@Column()
	firstName!: string

	@Field()
	@Column()
	lastName!: string

	@Field()
	@Column()
	birthDate!: Date

	@Field(type => BloodType)
	@Column({
		type: 'enum',
		enum: BloodType
	})
	bloodType!: BloodType

	@Field()
	@Column()
	organDonor!: boolean

	@Field(type => [Allergy])
	@OneToMany(type => Allergy, allergy => allergy.patient, { lazy: true })
	allergies!: Lazy<Allergy[]>

	@Field(type => [Vaccine])
	@OneToMany(type => Vaccine, vaccine => vaccine.patient, { lazy: true })
	vaccines!: Lazy<Vaccine[]>

	@Field(type => [Prescription])
	@OneToMany(type => Prescription, prescription => prescription.patient, { lazy: true })
	prescriptions!: Lazy<Prescription[]>

	@Field(type => [PatientDoctor])
	@OneToMany(type => PatientDoctor, relationship => relationship.patient, { lazy: true })
	doctorRelations!: Lazy<PatientDoctor[]>

	@Field(type => [Diagnosis])
	@OneToMany(type => Diagnosis, diagnosis => diagnosis.patient, { lazy: true })
	diagnosis!: Lazy<Diagnosis[]>
}

@InputType()
export class PatientInput implements Partial<Patient> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field()
	firstName!: string

	@Field()
	lastName!: string

	@Field(type => Date)
	birthDate!: Date

	@Field(type => BloodType)
	bloodType!: BloodType

	@Field()
	organDonor!: boolean
}

@InputType()
export class PatientId implements Partial<Patient> {
	@Field(type => ID)
	id!: string
}

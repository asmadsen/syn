import { registerEnumType } from 'type-graphql'

export enum DiagnosisCategory {
	Heart = 'Heart',
	Brain = 'Brain',
	Intestines = 'Intestines'
}

registerEnumType(DiagnosisCategory, {
	name: 'DiagnosisCategory'
})

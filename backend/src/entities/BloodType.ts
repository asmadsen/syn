import { registerEnumType } from 'type-graphql'

export enum BloodType {
	APositive = 'A+',
	ANegative = 'A-',
	BPositive = 'B+',
	BNegative = 'B-',
	OPositive = 'O+',
	ONegative = 'O-',
	ABPositive = 'AB+',
	ABNegative = 'AB-'
}

registerEnumType(BloodType, {
	name: 'BloodType',
	description: 'Enum containing the different blood types'
})

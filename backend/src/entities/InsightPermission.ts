import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, ID, Field, InputType } from 'type-graphql'
import { PatientDoctor } from './PatientDoctor'
import { Lazy } from '../helpers/Lazy'
import { InsightCategory } from './InsightCategory'

@Entity()
@ObjectType()
export class InsightPermission {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field(type => PatientDoctor)
	@ManyToOne(type => PatientDoctor, relationship => relationship.permissions, { lazy: true })
	patientDoctor!: Lazy<PatientDoctor> | string

	@Field(type => InsightCategory)
	@Column({
		type: 'enum',
		enum: InsightCategory
	})
	category!: InsightCategory

	@Field()
	@Column()
	grantedAt!: Date
}

@InputType()
export class InsightPermissionInput implements Partial<InsightPermission> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field(type => String)
	patientDoctor!: Lazy<PatientDoctor>

	@Field(type => InsightCategory)
	category!: InsightCategory

	@Field({ nullable: true, defaultValue: new Date() })
	grantedAt!: Date
}

import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { VaccineStatus } from './VaccineStatus'
import { Column } from 'typeorm/decorator/columns/Column'
import { Patient } from './Patient'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class Vaccine {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@ManyToOne(type => Patient, patient => patient.vaccines, { lazy: true })
	@Field(type => Patient)
	patient!: Lazy<Patient>

	@Field(type => VaccineStatus)
	@Column({
		type: 'enum',
		enum: VaccineStatus
	})
	status!: VaccineStatus

	@Field(type => [Date])
	@Column({
		type: 'simple-array',
		transformer: {
			from(value: string[]): Date[] {
				return value.map(value => new Date(Date.parse(value)))
			},
			to(value: Date[]): string[] {
				return value.map(value => value.toISOString())
			}
		}
	})
	vaccinationPlan!: Date[]

	@Field()
	@Column()
	name!: string

	@Field()
	@Column()
	description!: string

	@Field({ defaultValue: null, nullable: true })
	@Column({ nullable: true })
	expiresAt!: Date
}

@InputType()
export class VaccineInput implements Partial<Vaccine> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field(type => String)
	patient!: Lazy<Patient>

	@Field(type => VaccineStatus, { defaultValue: VaccineStatus.Ongoing })
	status!: VaccineStatus

	@Field(type => [Date], { defaultValue: [] })
	vaccinationPlan!: Date[]

	@Field()
	name!: string

	@Field()
	description!: string

	@Field({ defaultValue: null, nullable: true })
	expiresAt!: Date
}

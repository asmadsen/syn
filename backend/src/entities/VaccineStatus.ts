import { registerEnumType } from 'type-graphql'

export enum VaccineStatus {
	Renewable = 'Renewable',
	Expired = 'Expired',
	Permanent = 'Permanent',
	Ongoing = 'Ongoing'
}

registerEnumType(VaccineStatus, {
	name: 'VaccineStatus',
	description: 'Describes the state of a given vaccine'
})

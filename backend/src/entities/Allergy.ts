import { Entity, PrimaryGeneratedColumn } from 'typeorm'
import { Field, ID, InputType, ObjectType } from 'type-graphql'
import { Column } from 'typeorm/decorator/columns/Column'
import { AllergySeverity } from './AllergySeverity'
import { ManyToOne } from 'typeorm/decorator/relations/ManyToOne'
import { Patient } from './Patient'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class Allergy {
	@PrimaryGeneratedColumn('uuid')
	@Field(type => ID)
	id!: string

	@ManyToOne(type => Patient, patient => patient.allergies, { lazy: true })
	@Field(type => Patient)
	patient!: Lazy<Patient>

	@Column()
	@Field()
	name!: string

	@Column()
	@Field()
	allergicTo!: string

	@Column({
		type: 'enum',
		enum: AllergySeverity
	})
	@Field(type => AllergySeverity)
	allergySeverity!: AllergySeverity

	@Column()
	@Field()
	detected!: Date

	@Column()
	@Field()
	lastTested!: Date

	@Column()
	@Field()
	reaction!: string
}

@InputType()
export class AllergyInput implements Partial<Allergy> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field(type => String)
	patient!: Lazy<Patient>

	@Field()
	name!: string

	@Field()
	allergicTo!: string

	@Field(type => AllergySeverity)
	allergySeverity!: AllergySeverity

	@Field()
	detected!: Date

	@Field()
	lastTested!: Date

	@Field()
	reaction!: string
}

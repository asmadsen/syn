import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Patient } from './Patient'
import { Lazy } from '../helpers/Lazy'
import { DiagnosisCategory } from './DiagnosisCategory'

@Entity()
@ObjectType()
export class Diagnosis {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field(type => Patient)
	@ManyToOne(type => Patient, patient => patient.diagnosis, { lazy: true })
	patient!: Lazy<Patient>

	@Field()
	@Column()
	name!: string

	@Field(type => DiagnosisCategory)
	@Column({
		type: 'enum',
		enum: DiagnosisCategory
	})
	category!: DiagnosisCategory

	@Field()
	@Column()
	dateOfDiagnosis!: Date

	@Field()
	@Column()
	description!: string
}

@InputType()
export class DiagnosisInput implements Partial<Diagnosis> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field(type => String)
	patient!: Lazy<Patient>

	@Field()
	name!: string

	@Field(type => DiagnosisCategory)
	category!: DiagnosisCategory

	@Field()
	dateOfDiagnosis!: Date

	@Field()
	description!: string
}

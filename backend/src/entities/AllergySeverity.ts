import { registerEnumType } from 'type-graphql'

export enum AllergySeverity {
	MildAllergy = 'Mild Allergy',
	ModerateAllergy = 'Moderate Allergy',
	SevereAllergy = 'Severe Allergy'
}

registerEnumType(AllergySeverity, {
	name: 'AllergySeverity',
	description: 'The severity of allergy or intolerance'
})

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { Patient } from './Patient'
import { Lazy } from '../helpers/Lazy'

@Entity()
@ObjectType()
export class Prescription {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field(type => Patient)
	@ManyToOne(type => Patient, patient => patient.prescriptions, { lazy: true })
	patient!: Lazy<Patient>

	@Field()
	@Column()
	name!: string

	@Field()
	@Column()
	description!: string

	@Field()
	@Column()
	isMedicationStrong!: boolean

	@Field()
	@Column()
	issueDate!: Date

	@Field()
	@Column()
	expirationDate!: Date

	@Field()
	@Column()
	dosageDirections!: string
}

@InputType()
export class PrescriptionInput implements Partial<Prescription> {
	@Field(type => ID, { nullable: true })
	id!: string

	@Field(type => String)
	patient!: Lazy<Patient>

	@Field()
	name!: string

	@Field()
	description!: string

	@Field()
	isMedicationStrong!: boolean

	@Field()
	issueDate!: Date

	@Field()
	expirationDate!: Date

	@Field()
	dosageDirections!: string
}

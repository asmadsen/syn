import { registerEnumType } from 'type-graphql'

export enum InsightCategory {
	Allergy = 'Allergy',
	Diagnosis = 'Diagnosis',
	Prescription = 'Prescription',
	Vaccine = 'Vaccine'
}

registerEnumType(InsightCategory, {
	name: 'InsightCategory',
	description: 'Which category of data insight is granted for'
})

import { Entity } from 'typeorm/decorator/entity/Entity'
import { ManyToOne, OneToMany, PrimaryGeneratedColumn, Index } from 'typeorm'
import { Column } from 'typeorm/decorator/columns/Column'
import { Patient } from './Patient'
import { ObjectType, ID, Field } from 'type-graphql'
import { Lazy } from '../helpers/Lazy'
import { Doctor } from './Doctor'
import { Unique } from 'typeorm/decorator/Unique'
import { InsightPermission } from './InsightPermission'

@Entity()
@ObjectType()
@Unique(['patient', 'doctor'])
export class PatientDoctor {
	@Field(type => ID)
	@PrimaryGeneratedColumn('uuid')
	id!: string

	@Field(type => Patient)
	@ManyToOne(type => Patient, patient => patient.doctorRelations, { lazy: true })
	patient!: Lazy<Patient> | string

	@Field(type => Doctor)
	@ManyToOne(type => Doctor, doctor => doctor.patientRelations, { lazy: true })
	doctor!: Lazy<Doctor> | string

	@Field()
	@Column()
	relationship!: string

	@Field(type => [InsightPermission])
	@OneToMany(type => InsightPermission, permission => permission.patientDoctor, { lazy: true })
	permissions!: Lazy<InsightPermission[]>
}

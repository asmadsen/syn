export const typeorm = {
	name: 'default',
	type: 'postgres',
	driver: 'postgres',
	host: 'localhost',
	port: 5432,
	username: 'postgres',
	password: 'admin',
	database: 'syn',
	synchronize: false,
	entities: ['src/entities/**/*.ts'],
	migrations: ['migrations/**/*.ts'],
	cli: {
		entitiesDir: 'src/entities',
		migrationsDir: 'migrations'
	}
}

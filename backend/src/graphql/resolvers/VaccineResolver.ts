import { ResolverService } from '@tsed/graphql'
import { Vaccine, VaccineInput } from '../../entities/Vaccine'
import { VaccineService } from '../../services/VaccineService'
import { Arg, Mutation, Query } from 'type-graphql'

@ResolverService(Vaccine)
export class VaccineResolver {
	constructor(private vaccineService: VaccineService) {}

	@Mutation(type => Vaccine)
	async createVaccine(@Arg('vaccine') vaccineInput: VaccineInput): Promise<Vaccine> {
		const vaccine = new Vaccine()
		vaccine.id = vaccineInput.id
		vaccine.patient = vaccineInput.patient
		vaccine.status = vaccineInput.status
		vaccine.vaccinationPlan = vaccineInput.vaccinationPlan
		vaccine.name = vaccineInput.name
		vaccine.description = vaccineInput.description
		vaccine.expiresAt = vaccineInput.expiresAt
		return this.vaccineService.repository.save(vaccine)
	}

	@Query(returns => [Vaccine])
	async vaccines(@Arg('patientId') patientId: string): Promise<Vaccine[]> {
		return this.vaccineService.repository.find({
			where: {
				patient: patientId
			}
		})
	}
}

import { ResolverService } from '@tsed/graphql'
import { PatientDoctor } from '../../entities/PatientDoctor'
import { RepositoryService } from '../../services/RepositoryService'
import { Repository } from 'typeorm'
import { Mutation, Arg } from 'type-graphql'

@ResolverService(PatientDoctor)
export class PatientDoctorResolver {
	constructor(private repositoryService : RepositoryService) {}

	@Mutation(type => PatientDoctor)
	async createPatientDoctorRelationship(
		@Arg('patientId') patientId: string,
		@Arg('doctorId') doctorId: string,
		@Arg('relationship') relationship: string) : Promise<PatientDoctor> {
		const patientDoctor = new PatientDoctor()
		patientDoctor.patient = patientId
		patientDoctor.doctor = doctorId
		patientDoctor.relationship = relationship
		return this.repositoryService.getRepository(PatientDoctor).save(patientDoctor)
	}
}

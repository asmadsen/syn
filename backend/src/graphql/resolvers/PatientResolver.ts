import { ResolverService } from '@tsed/graphql'
import { Patient, PatientInput } from '../../entities/Patient'
import { PatientService } from '../../services/PatientService'
import { Arg, Mutation, Query } from 'type-graphql'

@ResolverService(Patient)
export class PatientResolver {
	constructor(private patientService: PatientService) {}

	@Mutation(type => Patient)
	async createPatient(@Arg('patient') inputPatient: PatientInput): Promise<Patient> {
		const patient = new Patient()
		patient.id = inputPatient.id
		patient.firstName = inputPatient.firstName
		patient.lastName = inputPatient.lastName
		patient.birthDate = inputPatient.birthDate
		patient.organDonor = inputPatient.organDonor
		patient.bloodType = inputPatient.bloodType
		return this.patientService.create(patient)
	}

	@Mutation(type => Patient)
	async changeOrganDonorState(@Arg('patientId') patientId: string, @Arg('organDonor') organDonor: boolean): Promise<Patient> {
		return this.patientService.changeOrganDonorState(patientId, organDonor)
	}

	@Query(returns => [Patient])
	async patients() {
		return this.patientService.repository.find()
	}
}

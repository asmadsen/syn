import { ResolverService } from '@tsed/graphql'
import { PrescriptionService } from '../../services/PrescriptionService'
import { Mutation, Arg, Query } from 'type-graphql'
import { Prescription, PrescriptionInput } from '../../entities/Prescription'

@ResolverService(Prescription)
export class PrescriptionResolver {
	constructor(private prescriptionService: PrescriptionService) {}

	@Mutation(type => Prescription)
	async createPrescription(@Arg('prescription') prescriptionInput: PrescriptionInput): Promise<Prescription> {
		const prescription = new Prescription()
		prescription.id = prescriptionInput.id
		prescription.patient = prescriptionInput.patient
		prescription.name = prescriptionInput.name
		prescription.description = prescriptionInput.description
		prescription.isMedicationStrong = prescriptionInput.isMedicationStrong
		prescription.issueDate = prescriptionInput.issueDate
		prescription.expirationDate = prescriptionInput.expirationDate
		prescription.dosageDirections = prescriptionInput.dosageDirections
		return this.prescriptionService.repository.save(prescription)
	}

	@Query(type => [Prescription])
	async prescriptions(@Arg('patientId') patientId: string): Promise<Prescription[]> {
		return this.prescriptionService.repository.find({
			where: {
				patient: patientId
			}
		})
	}
}

import { ResolverService } from '@tsed/graphql'
import { Doctor, DoctorInput } from '../../entities/Doctor'
import { Mutation, Arg, Query } from 'type-graphql'
import { RepositoryService } from '../../services/RepositoryService'

@ResolverService(Doctor)
export class DoctorResolver {
	constructor(private repositoryService: RepositoryService) {}

	@Mutation(type => Doctor)
	async createDoctor(@Arg('doctor') doctorInput: DoctorInput) {
		const doctor = new Doctor()
		doctor.id = doctorInput.id
		doctor.name = doctorInput.name
		doctor.associatedInstitution = doctorInput.associatedInstitution
		return this.repositoryService.getRepository(Doctor)
			.save(doctor)
	}

	@Query(type => [Doctor])
	async doctors() {
		return this.repositoryService.getRepository(Doctor)
			.find()
	}
}

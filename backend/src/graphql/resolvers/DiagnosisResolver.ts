import { ResolverService } from '@tsed/graphql'
import { Diagnosis, DiagnosisInput } from '../../entities/Diagnosis'
import { RepositoryService } from '../../services/RepositoryService'
import { Arg, Mutation, Query } from 'type-graphql'

@ResolverService(Diagnosis)
export class DiagnosisResolver {
	constructor(private repositoryService: RepositoryService) {}

	@Mutation(type => Diagnosis)
	async createDiagnosis(@Arg('diagnosis') diagnosisInput: DiagnosisInput) : Promise<Diagnosis> {
		const diagnosis = new Diagnosis()
		diagnosis.id = diagnosisInput.id
		diagnosis.patient = diagnosisInput.patient
		diagnosis.name = diagnosisInput.name
		diagnosis.category = diagnosisInput.category
		diagnosis.dateOfDiagnosis = diagnosisInput.dateOfDiagnosis
		diagnosis.description = diagnosisInput.description
		return this.repositoryService.getRepository(Diagnosis)
			.save(diagnosis)
	}

	@Query(type => [Diagnosis])
	async diagnosis(@Arg('patientId') patientId : string): Promise<Diagnosis[]> {
		return this.repositoryService.getRepository(Diagnosis)
			.find({
				where: {
					patient: patientId
				}
			})
	}
}

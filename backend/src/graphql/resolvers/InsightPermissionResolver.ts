import { ResolverService } from '@tsed/graphql'
import { InsightPermission, InsightPermissionInput } from '../../entities/InsightPermission'
import { RepositoryService } from '../../services/RepositoryService'
import { Arg, Mutation, Query } from 'type-graphql'

@ResolverService(InsightPermission)
export class InsightPermissionResolver {
	constructor(private repositoryService: RepositoryService) {}

	@Mutation(type => [InsightPermission])
	async givePermission(@Arg('permission') permissionInput : InsightPermissionInput) {
		const repo = this.repositoryService.getRepository(InsightPermission)
		const permissions = await repo.find({
			where: {
				patientDoctor: permissionInput.patientDoctor,
				category: permissionInput.category,
				grantedAt: permissionInput.grantedAt
			}
		})
		if (permissions.length === 0) {
			const permission = new InsightPermission()
			permission.patientDoctor = permissionInput.patientDoctor
			permission.category = permissionInput.category
			permission.grantedAt = permissionInput.grantedAt
			await repo.save(permission)
		}
		return repo.find({
			where: {
				patientDoctor: permissionInput.patientDoctor
			}
		})
	}

	@Mutation(type => [InsightPermission])
	async revokePermission(@Arg('permission') permission : InsightPermissionInput) {
		const repo = this.repositoryService.getRepository(InsightPermission)
		await repo.delete({
			patientDoctor: permission.patientDoctor,
			category: permission.category
		})

		return repo.find({
			where: {
				patientDoctor: permission.patientDoctor
			}
		})
	}
}

import { ResolverService } from '@tsed/graphql'
import { Allergy, AllergyInput } from '../../entities/Allergy'
import { AllergyService } from '../../services/AllergyService'
import { Arg, Mutation, Query } from 'type-graphql'

@ResolverService(Allergy)
export class AllergyResolver {
	constructor(private allergyService: AllergyService) {}

	@Mutation(type => Allergy)
	async createAllergy(@Arg('allergy') inputAllergy: AllergyInput): Promise<Allergy> {
		const allergy = new Allergy()
		allergy.id = inputAllergy.id
		allergy.patient = inputAllergy.patient
		allergy.name = inputAllergy.name
		allergy.allergicTo = inputAllergy.allergicTo
		allergy.allergySeverity = inputAllergy.allergySeverity
		allergy.detected = inputAllergy.detected
		allergy.lastTested = inputAllergy.lastTested
		allergy.reaction = inputAllergy.reaction
		return this.allergyService.create(allergy)
	}

	@Query(type => [Allergy])
	async allergies(@Arg('patientId') patientId: string) {
		return this.allergyService.findAllByPatientId(patientId)
	}
}
